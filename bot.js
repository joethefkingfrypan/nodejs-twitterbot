var fs = require('fs');
var async = require('async');
var Utils = require('./lib/utils.js');
var Regexer = require('./lib/regex.js');
var colors = require('colors/safe');
var BigNumber = require('bignumber.js');
var Twit = require('./node_modules/twit/lib/twitter');
var Bot = require('./node_modules/twit/examples/bot'), config1 = require('./config/configTweeter');
var BotConfig = require("./config/configBot.js");

/*+---------------------------+*
 *|           SETUP           |*
 /*+--------------------------+*/

var bot = new Bot(config1);
var regexer = new Regexer(tweetResponse);
var botScreenName = BotConfig.bot_screen_name;
var saveFile = BotConfig.bot_save_file;
var production_environment = BotConfig.production_environment;

Utils.displayTitle('TweeterBot has started');
if(production_environment) {
    runAutomation();
} else {
    runLocalAutomation();
}

/*+-----------------------------------------+*
 *|           TESTING ENVIRONMENT           |*
 /*+----------------------------------------+*/

// Run the whole automation by generating a random query without using the Twitter API
function runLocalAutomation() {
    simulateQuery();            //First run right away
    setInterval(function() {    //Then one run per 15 secs
        simulateQuery();
    }, 15000);
}

function simulateQuery() {
    Utils.displaySubtitle("ANSWERING A RANDOM TWEET");
    var tweetID = 42;
    var targetName = "@user";
    var tweetText = generateRandomQuery();
    Utils.displayString("%s said %s",
        {value:targetName, color:"green"},
        {value:tweetText,  color:"cyan"}
    );
    regexer.handleQuery(targetName,tweetText,tweetID);
}

/*+--------------------------------------------+*
 *|           PRODUCTION ENVIRONMENT           |*
 /*+-------------------------------------------+*/

// Run the whole automation using the Twitter API
function runAutomation() {
    handleNewTweets();          //First run right away
    setInterval(function() {    //Then one run per 1.25 min
        handleNewTweets();
    }, 75000);
}

function handleNewTweets() {
    var statusLimit, mentionLimit, followersLimit;
    var followersArray, dataFromTimeline;
    var limitsReached = false;

    var lastID = readLastIdFromFile(saveFile);
    lastID = lastID + 1;    //Adding "+1" to lastID because lastID has already been answered

    async.series({

            // Check API limits
            limitsCheck: function(callback){
                bot.twit.get('application/rate_limit_status', function(err, reply) {
                    if (err) return handleError(err);
                    statusLimit = getLimitFromApi(reply, 'application', 'rate_limit_status');
                    mentionLimit = getLimitFromApi(reply, 'statuses', 'mentions_timeline');
                    followersLimit = getLimitFromApi(reply, 'followers', 'ids');
                    callback(null, 1);
                });
            },

            // Retrieve timeline if limits have not been reached
            getTimeline: function(callback){
                if(mentionLimit > 0 && followersLimit > 0) {
                    Utils.displaySubtitle("RETRIEVING LAST TWEETS");
                    var options = { screen_name: botScreenName, count: 50, since_id: lastID};
                    bot.twit.get('statuses/mentions_timeline', options , function(err, data) {
                        if(err) return handleError(err);
                        dataFromTimeline = data;
                        Utils.displayString("%s refering to %s has been %s",
                            {value:'Mention timeline', color:"cyan"},
                            {value:'@' + botScreenName, color:"cyan"},
                            {value:'SUCCESSFULLY RETRIEVED', color:"green"}
                        );
                        callback(null, 2);
                    });
                } else {
                    limitsReached = true;
                    Utils.displayString("Impossible to %s as %s have been reached : %s",
                        {value:'run automation', color:"red"},
                        {value:'limits', color:"red"},
                        {value:'postponing', color:"cyan"}
                    );
                    callback(null, 2);
                }
            },

            // Retrieve followers list
            followersId: function(callback) {
                if(!limitsReached) {
                    Utils.displaySubtitle("RETRIEVING FOLLOWERS ID");
                    bot.twit.get('followers/ids', {}, function(err, dataFollowers) {
                        followersArray = dataFollowers['ids'];
                        Utils.displayString("%s has been %s",
                            {value:'Followers array', color:"cyan"},
                            {value:'SUCCESSFULLY RETRIEVED', color:"green"},
                            {value:'postponing', color:"cyan"}
                        );
                        callback(null, 3);
                    });
                } else {
                    callback(null, 3);
                }
            },

            // Answer all tweets if any
            analizingTweets: function(callback) {
                Utils.displaySubtitle("ANSWERING TO AVAILABLE TWEETS");
                if(dataFromTimeline.length === 0) {
                    Utils.displayString("%s have been %s",
                        {value:'All tweets', color:"cyan"},
                        {value:'SUCCESSFULLY ANSWERED', color:"green"}
                    );
                    callback(null, 4);
                } else {
                    async.eachSeries(dataFromTimeline,function(tweet, callback) {
                        var tweetID = tweet['id_str'];
                        var tweetText = tweet['text'];
                        var targetID = tweet['user']['id'];
                        var targetName = '@' + tweet['user']['screen_name'];
                        if(tweetID > lastID && Utils.inArray(targetID,followersArray)) {
                            Utils.displayTinySubtitle("ANSWERING " + targetName);
                            Utils.displayString("%s said %s",
                                {value:targetName, color:"green"},
                                {value:tweetText,  color:"cyan"}
                            );
                            regexer.handleQuery(targetName,tweetText,tweetID);
                        } else if(!production_environment) {
                            Utils.displayString("%s tweet from %s : already answered or the user is not a follower (ID %s)",
                                {value:'Skipping', color:"red"},
                                {value:targetName, color:"cyan"},
                                {value:tweetID, color:"green"}
                            );
                        }
                        callback(null, 4);
                    });
                }
            }
    },
    function(err, results) {
        if(err) return handleError(err);
    });
}

/*+---------------------------+*
 *|         FUNCTIONS         |*
 *+---------------------------+*/


// Tweet the given response referring to screenName and responding to given tweetID
function tweetResponse(response, screenName, tweetID) {
    if(response === null) {
        Utils.displayString("Query %s any %s : %s\n",
            {value:"did not match", color:"red"},
            {value:"regex", color:"cyan"},
            {value:"generating default answer", color:"green"}
        );
        response = Utils.generateTweetResponse("Hi %s, thanks for contacting me. Feel free to ask me questions about %s, or music related stuff (%s/%s) =)",
            {value:screenName, color:"green"},
            {value:"cities", color:"cyan"},
            {value:"songs", color:"cyan"},
            {value:"artists", color:"cyan"}
        );
    }
    Utils.displayString("%s is '%s'\n",
        {value:"Bot answer", color:"green"},
        {value:response, color:"cyan"}
    );

    if(production_environment) {
        bot.twit.post('statuses/update', { status: response,  in_reply_to_status_id: tweetID}, function(err, dataTweet, response) {
            saveLastIdToFile(saveFile,tweetID);
            if(err) return handleError(err);
        })
    }
}

function generateRandomQuery() {
    var queries = [
        //RANDOM QUERIES
        "Hey @joethefry, how are you?",
        "Hey @joethefry, what's up ?",

        //LASTFM
        "Hey @joethefry, what can you tell me about the song Shatter Me by Lindsey Stirling ?",     // Existing song + artist (spelled correctly)
        "Hey @joethefry, what can you tell me about the artist The Lonely Island.",                 // Existing artist (spelled correctly)
        "Hey @joethefry, what can you tell me about the artist Romain GAGNAIRE.",                   // Nonexistent artist
        "Hey @joethefry, do you know any song similar to Blackheart by Two Steps From Hell ?",      // Existing song + artist (spelled correctly)
        "Hey @joethefry, do you know any artist similar to Parov Stelar ???",                       // Existing artist
        "Hey @joethefry, do you know any artist similar to Parov Stellar ???",                      // Existing artist (misspelled)
        "Hey @joethefry, what are the most popular songs by Tove Lo",
        "Hey @joethefry, what are the most popular artists right now?",
        "Hey @joethefry, what are the most popular songs right now?",

         //SPARQL
         "Hey @joethefry, what can you tell me about Paris",
         "Hey @joethefry, what can you tell me about the city of Troyes ???",
         "Hey @joethefry, what can you tell me about Toulouse",
         "Hey @joethefry, what can you tell me about the city of  Tokyo",
         "Hey @joethefry, what can you tell me about Mexico City?",
         "Hey @joethefry, what can you tell me about London"
    ];
    return queries[Math.floor(Math.random()*queries.length)];
}

function getLimitFromApi(reply, category, api) {
    var limit = reply['resources'][category]['/' + category + '/' + api]['limit'];
    var remaining = reply['resources'][category]['/' + category + '/' + api]['remaining'];
    Utils.displayString('%s can be used %s more times (out of %s)',
        {value:api.toUpperCase(), color:"cyan"},
        {value:remaining, color:"green"},
        {value:limit, color:"green"}
    );
    return parseInt(remaining);
}

function handleError(err) {
    Utils.displayString("%s: %s",
        {value:'Response status:', color:"red"},
        {value:err['error']}
    );
    Utils.displayString("%s: %s",
        {value:'Error:', color:"red"},
        {value:JSON.stringify(err)}
    );
}

/*+-------------------------------+*
 *|         I/O FUNCTIONS         |*
 *+-------------------------------+*/

function readLastIdFromFile(file) {
    try {
        Utils.displaySubtitle("LOADING LAST ID ANSWERED");
        var lastId = new BigNumber("" + fs.readFileSync(file, 'utf8'));     //Forcing loading value to be a string
        Utils.displayString('Successfully loaded %s answered: %s',
            {value:'last ID', color:"cyan"},
            {value:lastId, color:"green"}
        );
        return lastId;
    } catch(e) {
        Utils.displayString('Save file %s, generating default ID (%s)',
            {value:'could not be found', color:"red"},
            {value:1, color:"green"}
        );
        return 1;
    }
}

function saveLastIdToFile(file,data) {
    fs.writeFileSync(file, data, function(err) {
        if(err) return handleError(err);
        Utils.displayString('Last %s updated : %s',
            {value:'ID answered', color:"cyan"},
            {value:data, color:"green"}
        );
    });
}