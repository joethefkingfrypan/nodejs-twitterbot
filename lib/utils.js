var colors = require('colors/safe');

module.exports = {

    // Check if needle is in array
    inArray: function(needle, haystack) {
        return haystack.indexOf(needle) > -1
    },

    getStringFromDate: function() {
        var d = new Date(Date.now());
        return d.getUTCFullYear()   + '-'
            +  (d.getUTCMonth() + 1) + '-'
            +   d.getDate();
    },

    /*+-------------------------------+*
     *|         LOG FUNCTIONS         |*
     *+-------------------------------+*/

    // Display title in a cool way
    displayTitle: function(text) {
        var border = "";
        for(var i=0; i<(text.length+10); i++) {
            border += "-";
        }
        console.log('\n+%s+',border);
        console.log('|     %s     |', colors.magenta.bold(text));
        console.log('+%s+\n',border);
    },

    // Display a tiny yellow title
    displaySubtitle: function(text) {
        console.log('\n%s\n', colors.yellow.bold(' ---- ' + text + ' ----'));
    },

    // Display a tiny grey title
    displayTinySubtitle: function(text) {
        console.log('\n%s\n', colors.gray('    ---- ' + text));
    },

    // Display string replacing every placeholders with values received as parameter
    displayString: function() {
        var consoleLog = arguments[0];
        for (var i = 1; i < arguments.length; ++i) {
            consoleLog = consoleLog.replace(/%s/,colorString(arguments[i]["value"],arguments[i]["color"]));
        }
        console.log("  " + consoleLog);
    },

    // Generate string replacing every placeholders with values received as parameter (for both display and tweet purposes)
    generateTweetResponse: function() {
        var tweetResponse, consoleLog;
        tweetResponse = consoleLog = arguments[0];
        for (var i = 1; i < arguments.length; ++i) {
            tweetResponse = tweetResponse.replace(/%s/,arguments[i]["value"]);
            consoleLog = consoleLog.replace(/%s/,colorString(arguments[i]["value"],arguments[i]["color"]));
        }
        console.log("  " + consoleLog);
        return tweetResponse;
    },

    // Generate string replacing every placeholders with values from array received as parameter (for both display and tweet purposes)
    generateTweetResponseFromArray: function() {
        var tweetResponse, consoleLog;
        tweetResponse = consoleLog = arguments[0];

        // Handle screenName first
        var screenName = arguments[1];
        tweetResponse = tweetResponse.replace(/%s/,screenName["value"]);
        consoleLog = consoleLog.replace(/%s/,colorString(screenName["value"],screenName["color"]));

        // Append parameters value if they fit
        var parametersArray = arguments[2];
        for (var i = 0, length = parametersArray.length; i < length; i++) {
            var expectedLength = parametersArray[i]["value"].length + tweetResponse.length + 2;     //Adding +2 for trailing coma and space
            if(expectedLength <= 140) {
                if(i > 0) {
                    tweetResponse += ", ";
                    consoleLog += ", ";
                }
                tweetResponse += parametersArray[i]["value"];
                consoleLog += colorString(parametersArray[i]["value"],parametersArray[i]["color"]);
            }
        }
        console.log("  " + consoleLog);
        return tweetResponse;
    }
};

// Utility function returning a colored string
function colorString(value,color) {
    switch (color) {
        case "green":
            return colors.green.bold(value);
            break;
        case "cyan":
            return colors.cyan.bold(value);
            break;
        case "yellow":
            return colors.yellow.bold(value);
            break;
        case "red":
            return colors.red.bold(value);
            break;
        case "blue":
            return colors.blue.bold(value);
            break;
        default:
            return value;
    }
}