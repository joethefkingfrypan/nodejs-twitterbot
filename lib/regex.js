var lastfm = require('./lastfm.js');
var sparql = require('./sparql.js');

/*+---------------------------------------------+*
 *|           CLASS - PATTERN MATCHER           |*
/*+---------------------------------------------+*/

// PatternMatcher constructor
var PatternMatcher = function (regex, callback) {
    this.data = null;
    this.regex = regex;
    this.callback = callback;
};

// Method allowing to run regex and check if there's a match
PatternMatcher.prototype.test = function(text) {
    this.data = this.regex.exec(text);
    return (null != this.data);
};

// Method allowing to run callback using parameters
PatternMatcher.prototype.runCallback = function(screenName,tweetCallback,tweetID) {
    this.callback(screenName,this.data,tweetCallback,tweetID);
};


/*+-------------------------------------+*
 *|           CLASS - REGEXER           |*
/*+-------------------------------------+*/

// Regexer constructor
var Regexer = function(tweetCallback) {
    this.tweetCallback = tweetCallback;
    this.patternMatchers = [
        generatePatternMatcherForDetailsFromArtist(),
        generatePatternMatcherForInfoFromTrack(),
        generatePatternMatcherForFindSimilarTracks(),
        generatePatternMatcherForFindSimilarArtist(),
        generatePatternMatcherForFindTopTracksFromArtist(),
        generatePatternMatcherForGetTopArtists(),
        generatePatternMatcherForGetLovedTracks(),
        generatePatternMatcherForGetInfoFromCity()
    ];
};

// Method allowing to handle a query sent by user
Regexer.prototype.handleQuery = function(screenName,text,tweetID) {
    for (i = 0; i < this.patternMatchers.length; i++) {
        if(this.patternMatchers[i].test(text)) {
            return this.patternMatchers[i].runCallback(screenName,this.tweetCallback,tweetID);
        }
    }
    // If no match, return null
    return this.tweetCallback(null,screenName,tweetID);
};


/*+-------------------------------------+*
 *|           MATCHERS SPARQL           |*
/*+-------------------------------------+*/

// Example of valid patterns
//    - "Hey @bot, what can you tell me about Paris"
//    - "Hey @bot, any info about the city of Troyes ???"
//    - "Hey @bot, any intel about Mexico City ?"
//    - "Hey @bot, what's cool about the city of Le Mans"
function generatePatternMatcherForGetInfoFromCity() {
    var regex_sparql_getInfoFromCity =
        '.*?' +	                        // Non-greedy match on filler
        'about(?: the city of){0,1}' +  // Keywords
        '\\s+' +	                    // Space
        '([a-zA-Z _]+)+' +	            // [1]Parameter (city name)
        '(\\s+.?)*';	                // Trailing question mark
    var regExp = new RegExp(regex_sparql_getInfoFromCity,["i"]);

    var getDataFromRegex = function(screenName,data,tweetCallback,tweetID) {
        var city = (data[1]).trim();
        sparql.getInfoFromCity(screenName,city,tweetCallback,tweetID);
    };

    return new PatternMatcher(regExp,getDataFromRegex);
}

/*+-------------------------------------+*
 *|           MATCHERS LASTFM           |*
/*+-------------------------------------+*/

// Example of valid patterns
//    - "Hey @bot, what can you tell me about the song Talking Body by Tove Lo?"
//    - "Hey @bot, what can you tell me about the track Talking Body by Tove Lo"
function generatePatternMatcherForInfoFromTrack() {
    var regex_lastFM_infoFromTrack =
        '^.*?' +	                    // Non-greedy match on filler
        'about the (?:song|track)' +    // Keywords
        '\\s+' +	                    // Space
        '([a-zA-Z ]+)+' +	            // [1]Parameter (song name)
        '\\s+' +	                    // Space
        'by' +                          // Keyword
        '\\s+' +	                    // Space
        '([a-zA-Z ]+)+' +	            // [2]Parameter (artist name)
        '(?:\\s+.?)*';	                // Trailing question mark
    var regExp = new RegExp(regex_lastFM_infoFromTrack,["i"]);

    var getDataFromRegex = function(screenName,data,tweetCallback,tweetID) {
        var track = (data[1]).trim();
        var artist = (data[2]).trim();
        lastfm.getInfoFromTrack(screenName,track,artist,tweetCallback,tweetID);
    };

    return new PatternMatcher(regExp,getDataFromRegex);
}


// Example of valid patterns
//    - "Hey @bot, what can you tell me about the artist Tove Lo."
function generatePatternMatcherForDetailsFromArtist() {
    var regex_lastFM_detailsFromArtist =
        '^.*?' +	                            // Non-greedy match on filler
        'about the (?:artist|group|singer)' +   // Keyword
        '\\s+' +	                            // Space
        '([a-zA-Z ]+)+' +	                    // [1]Parameter (artist name)
        '(?:\\s+?.)*';	                        // Trailing question mark
    var regExp = new RegExp(regex_lastFM_detailsFromArtist,["i"]);

    var getDataFromRegex = function(screenName,data,tweetCallback,tweetID) {
        var artist = (data[1]).trim();
        lastfm.getDetailsFromArtist(screenName,artist,tweetCallback,tweetID);
    };

    return new PatternMatcher(regExp,getDataFromRegex);
}


// Example of valid patterns
//    - "Hey @bot, do you know any song similar to Talking Body by Tove Lo ?"
//    - "Hey @bot, do you know any track similar to Talking Body by Tove Lo."
function generatePatternMatcherForFindSimilarTracks() {
    var regex_lastFM_findSimilarTracks =
        '^.*?' +	                        // Non-greedy match on filler
        'any (?:song|track) similar to' +   // Keywords
        '\\s+' +	                        // Space
        '([a-zA-Z ]+)+' +	                // [1]Parameter (song name)
        '\\s+' +	                        // Space
        'by' +                            // Keyword
        '\\s+' +	                        // Space
        '([a-zA-Z ]+)+' +	                // [2]Parameter (artist name)
        '(?:\\s+?.)*';	                    // Trailing question mark
    var regExp = new RegExp(regex_lastFM_findSimilarTracks,["i"]);

    var getDataFromRegex = function(screenName,data,tweetCallback,tweetID) {
        var track = (data[1]).trim();
        var artist = (data[2]).trim();
        lastfm.findSimilarTracks(screenName,track,artist,tweetCallback,tweetID);
    };

    return new PatternMatcher(regExp,getDataFromRegex);
}


// Example of valid patterns
//    - "Hey @bot, do you know any artist similar to Tove Lo ???"
function generatePatternMatcherForFindSimilarArtist() {
    var regex_lastFM_findSimilarArtist =
        '^.*?' +	                                // Non-greedy match on filler
        'any (?:artist|group|singer) similar to' +  // Keywords
        '\\s+' +	                                // Space
        '([a-zA-Z ]+)+' +	                        // [1]Parameter (artist name)
        '(?:\\s+?.)*';	                            // Trailing question mark
    var regExp = new RegExp(regex_lastFM_findSimilarArtist,["i"]);

    var getDataFromRegex = function(screenName,data,tweetCallback,tweetID) {
        var artist = (data[1]).trim();
        lastfm.findSimilarArtist(screenName,artist,tweetCallback,tweetID);
    };

    return new PatternMatcher(regExp,getDataFromRegex);
}


// Example of valid patterns
//    - "Hey @bot, what are the most popular songs from Tove Lo"
//    - "Hey @bot, what are the most popular tracks from Tove Lo?"
function generatePatternMatcherForFindTopTracksFromArtist() {
    var regex_lastFM_findTopTracksFromArtist =
        '^.*?' +	                            // Non-greedy match on filler
        'most popular (?:songs|tracks) by' +    // Keywords
        '\\s+' +	                            // Space
        '([a-zA-Z ]+)+' +	                    // [1]Parameter (artist name)
        '(?:\\s+?.)*';	                        // Trailing question mark
    var regExp = new RegExp(regex_lastFM_findTopTracksFromArtist,["i"]);

    var getDataFromRegex = function(screenName,data,tweetCallback,tweetID) {
        var artist = (data[1]).trim();
        lastfm.findTopTracksFromArtist(screenName,artist,tweetCallback,tweetID);
    };

    return new PatternMatcher(regExp,getDataFromRegex);
}


// Example of valid patterns
//    - "Hey @bot, what are the most popular artists right now?"
function generatePatternMatcherForGetTopArtists() {
    var regex_lastFM_getTopArtists =
        '^.*?' +	                                            // Non-greedy match on filler
        'most popular (?:artists|groups|singers) right now' +   // Keywords
        '(?:\\s+?)*';	                                        // Trailing question mark
    var regExp = new RegExp(regex_lastFM_getTopArtists,["i"]);

    var getDataFromRegex = function(screenName,data,tweetCallback,tweetID) {
        lastfm.getTopArtists(screenName,tweetCallback,tweetID);
    };

    return new PatternMatcher(regExp,getDataFromRegex);
}


// Example of valid patterns
//    - "Hey @bot, what are the most popular songs right now?"
//    - "Hey @bot, what are the most popular tracks right now ???"
function generatePatternMatcherForGetLovedTracks() {
    var regex_lastFM_getTopArtists =
        '^.*?' +	                                    // Non-greedy match on filler
        'most popular (?:songs|tracks) right now' +     // Keywords
        '(?:\\s+?)*';	                                // Trailing question mark
    var regExp = new RegExp(regex_lastFM_getTopArtists,["i"]);

    var getDataFromRegex = function(screenName,data,tweetCallback,tweetID) {
        lastfm.getLovedTracks(screenName,tweetCallback,tweetID);
    };

    return new PatternMatcher(regExp,getDataFromRegex);
}

module.exports = Regexer;