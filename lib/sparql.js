var SparqlClient = require('sparql-client');
var colors = require('colors/safe');
var Utils = require('./utils.js');
var Bitly = require('bitly');
var util = require('util');
var configBitly = require('../config/configBitly');

//API Keys
var bitly = new Bitly(configBitly.bitly_username, configBitly.bitly_api_key);
var client = new SparqlClient('http://dbpedia.org/sparql');

// Main query (using dbpedia-owl:Settlement)
function generateMainQuery() {
    return "SELECT * FROM <http://dbpedia.org> WHERE { " +
        "?city rdf:type dbpedia-owl:Settlement;" +
        "rdfs:label ?label;" +
        "dbpedia-owl:abstract ?abstract;" +
        "dbpedia-owl:populationTotal ?pop;" +
        "dbpedia-owl:country ?country; " +
        "dbpedia-owl:wikiPageExternalLink ?ext. " +
        "?country dbpprop:commonName ?coutryName; " +
        "dbpedia-owl:capital ?capital ." +
        "?capital dbpprop:location ?capitalName ." +
        "}LIMIT 1";
}

// Fallback query (using dbpedia-owl:City)
function generateFallbackQuery() {
    return "SELECT * FROM <http://dbpedia.org> WHERE {" +
        "?city rdf:type dbpedia-owl:City;" +
        "rdfs:label ?label; " +
        "dbpedia-owl:abstract ?abstract; " +
        "dbpedia-owl:populationTotal ?pop; " +
        "dbpedia-owl:country ?country; " +
        "dbpedia-owl:wikiPageExternalLink ?ext. " +
        "?country dbpprop:commonName ?coutryName; " +
        "dbpedia-owl:capital ?capital . " +
        "?capital dbpprop:location ?capitalName . " +
        "}LIMIT 1";
}

module.exports = {

    // Retrieve data from a specific CITY or SETTLEMENT
    getInfoFromCity: function(screenName, city, tweetCallback,tweetID) {

        cityQuery = fixCityName(city);
        client.query(generateMainQuery())
            .bind('city', '<http://dbpedia.org/resource/' + cityQuery + '>')
            .execute(function(err, data) {
                if(err) return handleError(err);

                // If no results, fallback to dbpedia-owl:City
                if(data['results']['bindings'].length === 0) {

                    client.query(generateFallbackQuery())
                        .bind('city', '<http://dbpedia.org/resource/' + cityQuery + '>')
                        .execute(function(err, fallbackdata) {
                            if(err) return handleError(err);
                            Utils.displaySubtitle("QUERYING DBPEDIA ABOUT " + city.toUpperCase());

                            // If still no results, blame the Wikipedia guys
                            if(fallbackdata['results']['bindings'].length === 0) {
                                var tweetAnswer = Utils.generateTweetResponse("Sorry %s, %s about %s. Its wiki page must %s",
                                    {value: screenName, color: "green"},
                                    {value: "I can't find anything", color: "red"},
                                    {value: city, color: "cyan"},
                                    {value: "lack key info", color: "red"}
                                );
                                tweetCallback(tweetAnswer,tweetID);
                            } else {
                                renderData(screenName,city,data['results']['bindings'][0],tweetCallback,tweetID);
                            }
                        });

                } else {
                    Utils.displaySubtitle("QUERYING DBPEDIA ABOUT " + city.toUpperCase());
                    renderData(screenName,city,data['results']['bindings'][0],tweetCallback,tweetID);
                }
            });
    }
};

// In DBPEDIA, "Mexico City" is referred as "Mexico_City", "Le Mans" as "Le_Mans", and so on
// So this function replace every space occurrence with a single underscore
function fixCityName(city) {
    return city.replace(/\s+/g,"_");
}

// Function used to generate response from retrieved data
function renderData(screenName, city, result, tweetCallback, tweetID) {
    var population = result['pop']['value'];
    var country = result['coutryName']['value'];
    var capital = result['capitalName']['value'];
    var URL = result['ext']['value'];

    bitly.shorten(URL, function(err, response) {
        if (err) throw err;
        var short_url = response.data.url;
        var tweetAnswer = Utils.generateTweetResponse("Hi %s, %s has %s inhabitants and is located in %s (Capital: %s). Find out more at %s",
            {value: screenName, color: "green"},
            {value: city,       color: "cyan"},
            {value: population, color: "cyan"},
            {value: country,    color: "cyan"},
            {value: capital,    color: "cyan"},
            {value: short_url,  color: "cyan"}
        );
        tweetCallback(tweetAnswer,screenName,tweetID);
    });
}

// Function used to generate response when there is no matching data
function handleError(screenName,err,tweetCallback,tweetID) {
    console.error('response status:', err['code']);
    console.error('details:', err);
    var tweetAnswer = Utils.generateTweetResponse("Sorry %s, I can't find any result for this city for now. Please try again in a few minutes (Not my fault, I swear) =)",
        {value:screenName, color:"green"}
    );
    tweetCallback(tweetAnswer,screenName,tweetID);
}