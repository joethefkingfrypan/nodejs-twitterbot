# README - TwitterBot [LO10] #

Ce bot twitter a été réalisé par [__Romain GAGNAIRE__](romgagnaire@gmail.com) dans le cadre de l'UV LO10 à l'Université de Technologies de Troyes au semestre de Printemps 2015.

### Comment le démarrer ? ###

Pour le démarrer le bot, il suffit d'exécuter la commande `node bot`. 

Il analysera tous les tweets qui lui sont adressés en ne répondant qu'aux utilisateurs qui font partie de ses abonnés, et uniquement s'il ne leur a pas déjà répondu.

__ATTENTION__ : veillez à bien prendre connaissance des étapes de __configuration préalable !__ 

### Quelles intégrations propose-t-il ? ###

* [Twitter](https://www.npmjs.com/package/twit) : pour des raisons évidentes.
* [Sparql](https://www.npmjs.com/package/sparql-client) : extraction de données depuis [DBPedia](http://wiki.dbpedia.org).
* [LastFM](https://www.npmjs.com/package/lastfmapi) : extraction de données depuis [LastFM](http://www.lastfm.fr).
* [Bitly](https://www.npmjs.com/package/bitly) : service de réduction d'URI disponbile à [cette adresse](https://bitly.com).

### À quelles bibliothèques tierces fait-il appel ? ###

- [Async](https://www.npmjs.com/package/async) : pour gérer l'ordonnacement et l'asynchronisme.
- [Bignumber](https://www.npmjs.com/package/bignumber.js) : pour pouvoir manipuler numériquement les ID des tweets.
- [Colors](https://www.npmjs.com/package/colors) : parce que la vie est plus jolie en couleurs.

### Quels sont les prérequis ? ###

###### Concernant Sparql ######

* Son utilisation ne nécessite pas de compte particulier.

###### Concernant Twitter ######

* Son utilisation nécessite l'obtention d'[identifiants d'application](https://apps.twitter.com).

###### Concernant LastFM ######

* Son utilisation nécessite l'obtention d'[identifiants d'application](http://www.lastfm.fr/api/account/create).

###### Concernant Bitly ######

* Son utilisation nécessite l'obtention d'[identifiants d'application](http://dev.bitly.com/my_apps.html).

###### Que faire ensuite ? ######

Une fois les identifiants obtenus :

1. Éditer les fichiers 
    - `config/configTweeter.js`
    - `config/configLastFM.js` 
    - `config/configBitly.js`
2. Remplaçer les valeurs présentes dans le fichier avec celles générées.
3. Éditer le fichier `config/configBot.js` et remplacer la valeur de `bot_screen_name` par le nom d'utilisateur Twitter de votre propre bot.

### Comment les sources sont-elles organisées  ? ###

    └── bot.js                  // Programme principal
    └── config
         └── configBot.js       // Fichier de configuration général du Bot   
         └── configBitly.js     // Fichier de configuration pour Bitly   
         └── configLastFM.js    // Fichier de configuration pour LastFM
         └── configTweeter.js   // Fichier de configuration pour Tweeter
    └── lib
         └── lastfm.js          // Implémentation de l'intégration avec LastFM
         └── regex.js           // Implémentation de l'analyse des requêtes
         └── sparql.js          // Implémentation de l'intégration avec Sparql
         └── utils.js           // Implémentation de fonctions utilitaires
    └── node_modules
         └── async          
         └── bignumber.js         
         └── bitly              // Le dossier "node_modules" contient 
         └── colors             // toutes les bibliothèques tierces
         └── lastfmapi           
         └── sparql-client        
         └── twit          
    └── .save                   // Fichier de sauvegarde

### Existe-t-il des limites d'utilisation de ces API ? ###

* [Twitter](https://www.npmjs.com/package/twit) : gérées automatiquement par la logique de l'application.
* [Sparql](https://www.npmjscom/package/sparql-client) : aucune limite particulière.
* [LastFM](https://www.npmjs.com/package/lastfmapi) : limité à un certain nombre d'appels par seconde.
* [Bitly](https://www.npmjs.com/package/bitly) : limité à un certain nombre d'appels par seconde.

__RÉSUMÉ__ : Les limites de LastFM et Bitly sont impossibles à atteindre compte tenu des limitations internes de Twitter.

### Comment fonctionne-t-il ? ###

- Le fichier `lib/regex.js` contient toutes les expressions régulières, qui sont relativement faciles à comprendre, ainsi que des exemples d'utilisation.
- Les requête valides sont la forme `<intro> <mot-clé> <valeur>`
- L'introduction est complètement libre

Exemples valides :

    Hey @bot :) <mot-clé> <valeur>
    Hi @bot ! <mot-clé> <valeur>
    What's up @bot? <mot-clé> <valeur>
    @bot, <mot-clé> <valeur>
    Oh hi @bot ! How are you doing ? <mot-clé> <valeur>

### Fonctionnement pour Sparql ###

* Le fichier `lib/sparql.js` contient tous les traitements associés à cette intégration.
* Toutes les requêtes contenant `about NOM_VILLE` ou `about the city of NOM_VILLE` déclencheront un recherche sur DBPEdia.

Exemples valides :

    Hey @bot, what can you tell me about Paris
    Hey @bot, any info about the city of Troyes ???
    Hey @bot, any intel about the city of Le Mans ?
    Hey @bot, what's about Mexico City.

Par exemple `Hey @bot, any info about the city of Marseille?` déclenchera une recherche sur DBPedia concernant la ville de Marseille et la réponse du Bot sera : 

    Hi @user, Marseille has 850726 inhabitants and is located in France (Capital: Paris). Find out more at http://bit.ly/1L6z2io

### Fonctionnement pour LastFM ###

* Le fichier `lib/lastfm.js` contient tous les traitements associés à cette intégration.
* Il est possible de demander au Bot plusieurs types d'information :
    - trouver les détails d'une chanson d'un artiste en particulier (`getInfoFromTrack`)
    - trouver les détails d'un artiste donné (`getDetailsFromArtist`)
    - trouver les chansons similaires à une piste donnée (`findSimilarTracks`)
    - trouver les artistes similaires à un artiste donné (`findSimilarArtist`)
    - trouver les morceaux les plus populaires d'un artiste donné (`findTopTracksFromArtist`)
    - trouver les artistes les plus populaires du moment (`getTopArtists`)
    - trouver les chansons les plus appréciées du moment (`getLovedTracks`)
* Ci-dessous sont listés des exemples de requêtes valides à noter que le mot-clé `song` est interchangeable avec `track`; tout comme le mot-clé `artist` est interchangeable avec `group` et `singer`.

Exemples valides :

    Hey @bot, what can you tell me about the song Shatter Me by Lindsey Stirling ?
    Hey @bot, what can you tell me about the artist The Lonely Island.
    Hey @bot, do you know any song similar to Blackheart by Two Steps From Hell ?
    Hey @bot, do you know any artist similar to Parov Stelar ???
    Hey @bot, what are the most popular songs by Tove Lo
    Hey @bot, what are the most popular artists right now?
    Hey @bot, what are the most popular songs right now?

### En quoi est-il différents des autres ? ###

###### Prise en compte des limites ######

* Tweeter limite l'utilisation de son API pour chacune de ses méthodes REST.
* Le bot intègre une fonctionnalité (`limitsCheck`) permettant de ne lancer le traitement que si les limites n'ont pas encore été atteintes.

###### Détection des requêtes ######

* Le bot utilise des expressions régulières pour détecter à quelle fonctionnalité l'utilisateur souhaite faire appel (`lib/regex.js`).
* Ces expressions régulières ont été pensées pour laisser un maximum de flexibilité à l'utilisateur.

###### Sauvegarde et persistence ######

* Le bot garde trace du dernier tweet auquel il a répondu par le biais d'un fichier de sauvegarde (`.save`) dans lequel il sauvegarde l'identifiant.
* Si ce fichier n'existe pas, il sera généré automatiquement en prenant un identifiant par défaut (`1`), permettant de répondre à tous les tweets existants.

###### Types d'environnements (développement .vs. production) ######

* Tester l'application avec Twitter est contraignant en raisons des limitations d'appel.
* Il est possible de les outrepasser en simulant le fonctionnement de l'application en mettant à __false__ le paramètre `production_environment` présent dans le fichier `config/configBot.js`.
* Une requête aléatoire parmi une liste définie sera récupérée toutes les 15 secondes et traitée aboutissant à l'affichage de la réponse du bot sans que le moindre appel à l'API de Twitter soit effectué (`generateRandomQuery`).
* __NOTE__ : Être en environnement de développement ne dispense pas d'obtenir des identifiants d'application pour les services LastFM et Bitly.
